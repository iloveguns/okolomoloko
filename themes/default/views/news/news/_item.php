<?php
/* @var $data News */
?>
<div class="project-item">
    <a href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]) ?>">
        <p><?= date('d.m.Y', strtotime($data->date)) ?></p>
        <h2><?= $data->title ?></h2>
        <img src="<?= $data->getImageUrl() ?>" alt="<?= $data->title ?>">
        <p><?= $data->short_text ?></p>
    </a>
    <a href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]) ?>" class="btn">Читать</a>
</div>
