<?php
$this->title = Yii::app()->getModule('news')->metaTitle ?: Yii::t('NewsModule.news', 'News');
$this->description = Yii::app()->getModule('news')->metaDescription;
$this->keywords = Yii::app()->getModule('news')->metaKeyWords;

$this->breadcrumbs = [Yii::t('NewsModule.news', 'News')];
?>

<h1>
    <a class="back" href="<?= Yii::app()->request->urlReferrer ?>"><img
                src="<?= $this->mainAssets ?>/img/icons/back.svg" alt="Назад"></a>
    <?= $this->title; ?>
</h1>

<?php $this->widget(
    'bootstrap.widgets.TbListView',
    [
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'template' => '{items} {pager}'
    ]
); ?>
