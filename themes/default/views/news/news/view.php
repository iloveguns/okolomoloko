<?php
/**
 * Отображение для ./themes/default/views/news/news/news.php:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     https://yupe.ru
 *
 * @var $this NewsController
 * @var $model News
 **/
?>
<?php
$this->title = $model->meta_title ?: $model->title;
$this->description = $model->meta_description;
$this->keywords = $model->meta_keywords;
?>

<?php
$this->breadcrumbs = [
    Yii::t('NewsModule.news', 'News') => ['/news/news/index'],
    $model->title
];
?>

<h1 class="contacts-header">
    <a class="back" href="<?= Yii::app()->request->urlReferrer ?>"><img
                src="<?= $this->mainAssets ?>/img/icons/back.svg" alt="Назад"></a>
    <?= $this->title; ?>
</h1>

<div class="catalog-item-page project-item-page news-item-page">
    <?php /*if ($model->image): ?>
        <?= CHtml::image($model->getImageUrl(), $model->title,
            ['class' => 'news-item-page-img']); ?>
    <?php endif;*/ ?>
    <p><?= date('d.m.Y', strtotime($model->date)) ?></p>
    <?= $model->full_text; ?>
</div>