<?php if ($dataProvider->itemCount): ?>
    <section class="homepage-slider">
        <div class="js-homepage-slider">
            <?php foreach ($dataProvider->getData() as $data): ?>
                <div class="homepage-slider-item">
                    <div class="homepage-slider-item-img">
                        <img src="<?= $data->image->getImageUrl(1120, 432) ?>" alt="">
                    </div>
                    <div class="text">
                        <h2><?= $data->image->name ?></h2>
                        <p><?= $data->image->description ?></p>
                        <a href="<?= $data->image->alt ?>" class="btn">Посмотреть</a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>
