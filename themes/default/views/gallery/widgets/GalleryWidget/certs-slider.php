<?php if ($dataProvider->itemCount): ?>
    <div class="cert js-cert-slider">
        <?php foreach ($dataProvider->getData() as $data): ?>
            <div class="cert-item">
                <a href="<?= $data->image->getImageUrl() ?>">
                    <img src="<?= $data->image->getImageUrl(374, 530) ?>" alt="<?= $data->image->name ?>">
                </a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
