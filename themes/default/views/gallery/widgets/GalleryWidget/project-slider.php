<?php if ($dataProvider->itemCount): ?>
    <div class="project-item-slider js-project-slider">
        <?php foreach ($dataProvider->getData() as $data): ?>
            <div><img src="<?= $data->image->getImageUrl() ?>" alt=""></div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
