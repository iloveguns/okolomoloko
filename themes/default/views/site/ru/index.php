<?php $this->title = Yii::app()->getModule('yupe')->siteName; ?>

<?php $this->widget('yupe\widgets\YFlashMessages'); ?>

<!-- Главный слайдер -->
<?php $this->widget(
    'gallery.widgets.GalleryWidget',
    ['galleryId' => 1, 'limit' => 5, 'view' => 'homepage-slider']
); ?>

<!-- О компании -->
<section class="homepage-about">
    <h2 class="section-header">О компании</h2>
    <p><?php $this->widget(
            "application.modules.contentblock.widgets.ContentBlockWidget",
            array("code" => "o-kompanii"));
        ?></p>
</section>

<!-- Каталог продукции -->
<section class="homepage-catalog">
    <?php
    $categoryRepository = new CategoryRepository();
    $catalogCategories = $categoryRepository->getDescendants(1);
    $catalogCategoriesChunks = array_chunk($catalogCategories, 4);
    ?>
    <h2 class="section-header">Продукция</h2>
    <div class="catalog">
        <?php foreach ($catalogCategoriesChunks as $catalogCategoriesChunk): ?>
            <div class="catalog-row">
                <?php foreach ($catalogCategoriesChunk as $catalogCategory): ?>
                    <a href="/katalog-produkcii?category=<?= $catalogCategory->id ?>" class="catalog-item">
                        <img src="<?= $catalogCategory->getImageUrl() ?>" alt="<?= $catalogCategory->name ?>">
                        <p><?= $catalogCategory->name ?></p>
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<section class="homepage-certs">
    <h2 class="section-header">Сертификаты</h2>
    <!-- Слайдер сертификатов -->
    <?php $this->widget(
        'gallery.widgets.GalleryWidget',
        ['galleryId' => 2, 'limit' => 30, 'view' => 'certs-slider']
    ); ?>
</section>
