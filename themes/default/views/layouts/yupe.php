<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START); ?>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta http-equiv="Content-Language" content="ru-RU"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <title><?= $this->title; ?></title>
    <meta name="description" content="<?= $this->description; ?>"/>
    <meta name="keywords" content="<?= $this->keywords; ?>"/>

    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>"/>
    <?php endif; ?>


    <?php
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/slick/slick.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/slick/slick-theme.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/scss/style.css');

    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/slick/slick.min.js');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.magnific-popup.min.js');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/script.js');
    ?>
    <script type="text/javascript">
        var yupeTokenName = '<?= Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?= Yii::app()->getRequest()->getCsrfToken();?>';
    </script>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END); ?>
</head>

<body>

<div class="mobile-menu js-mobile-menu">
    <p class="mobile-menu-description">Технологическое оборудование для молочной промышленности</p>

    <ul>
        <li><a href="/katalog-produkcii">Каталог продукции</a></li>
        <li><a href="/uslugi">Услуги</a></li>
        <li><a href="/realizovannye-proekty">Реализованные проекты</a></li>
        <li><a href="/news">Новости</a></li>
        <li><a href="/kontakty">Контакты</a></li>
    </ul>
</div>

<div class="container">
    <header class="header">
        <div class="row">
            <div class="header-logo-container">
                <a href="/">
                    <img class="header-logo" src="<?= $this->mainAssets ?>/img/logo/logo-desc.svg" alt="Логотип">
                </a>
            </div>
            <div class="header-mobile-container">
                <a href="tel:<?php $this->widget(
                    "application.modules.contentblock.widgets.ContentBlockWidget",
                    array("code" => "telefon"));
                ?>"><img src="<?= $this->mainAssets ?>/img/icons/phone.svg" alt="Телефон"></a>
                <a href="#" class="js-mobile-search"><img src="<?= $this->mainAssets ?>/img/icons/search.svg"
                                                          alt="Поиск"></a>
                <a href="#" class="js-burger"><img src="<?= $this->mainAssets ?>/img/icons/burger.svg" alt="Меню"></a>
            </div>
            <div class="header-phone-container">
                <a class="header-phone" href="tel:<?php $this->widget(
                    "application.modules.contentblock.widgets.ContentBlockWidget",
                    array("code" => "telefon"));
                ?>">
                    <?php $this->widget(
                        "application.modules.contentblock.widgets.ContentBlockWidget",
                        array("code" => "telefon"));
                    ?>
                </a>
                <a class="header-consult js-consult" href="#">
                    <img src="<?= $this->mainAssets ?>/img/icons/phone.svg" alt="Онлайн-консультант">
                    Онлайн-консультант
                </a>
            </div>
            <div class="header-desc-container">
                <p>Технологическое оборудование для молочной промышленности</p>
            </div>
        </div>
        <div class="row">
            <div class="header-search-container js-header-search">
                <form action="/search" method="get">
                    <input class="form-control" type="text" name="q">
                    <button type="submit" class="btn"><img src="<?= $this->mainAssets ?>/img/icons/search.svg"
                                                           alt="Поиск">
                    </button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="header-menu">
                <ul>
                    <li><a href="/katalog-produkcii">Каталог продукции</a></li>
                    <li><a href="/uslugi">Услуги</a></li>
                    <li><a href="/realizovannye-proekty">Реализованные проекты</a></li>
                    <li><a href="/news">Новости</a></li>
                    <li><a href="/kontakty">Контакты</a></li>
                </ul>
            </div>
        </div>
    </header>
</div>

<div class="container">
    <div class="row">
        <?= $content; ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="footer-logo-container">
                <a href="/">
                    <img src="<?= $this->mainAssets ?>/img/logo/logo.svg" alt="Логотип">
                </a>
            </div>
            <div class="footer-desc-container">
                <p>Технологическое оборудование для молочной промышленности</p>
            </div>
            <div class="footer-price-container">
                <a href="<?= $this->mainAssets ?>/docs/price.docx" download="Околомолоко.Прайс-лист.docx">
                    <img src="<?= $this->mainAssets ?>/img/icons/docs.svg" alt="">
                    <div>Скачать прайс-лист</div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="footer-contact-container">
                <a class="contact-link" href="tel:<?php $this->widget(
                    "application.modules.contentblock.widgets.ContentBlockWidget",
                    array("code" => "telefon"));
                ?>"><?php $this->widget(
                        "application.modules.contentblock.widgets.ContentBlockWidget",
                        array("code" => "telefon"));
                    ?></a><br>
                <a class="contact-link" href="mailto:<?php $this->widget(
                    "application.modules.contentblock.widgets.ContentBlockWidget",
                    array("code" => "email"));
                ?>"><?php $this->widget(
                        "application.modules.contentblock.widgets.ContentBlockWidget",
                        array("code" => "email"));
                    ?></a>
            </div>
            <div class="footer-address-container">
                <p><?php $this->widget(
                        "application.modules.contentblock.widgets.ContentBlockWidget",
                        array("code" => "adres"));
                    ?></p>
            </div>
        </div>
        <div class="row footer-developer">
            <a href="http://adelfo-studio.ru" target="_blank">Разработка сайта – Adelfo Development</a>
        </div>
    </div>
</footer>

</body>
</html>
