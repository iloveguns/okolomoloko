<?php
/* @var $model Page */
/* @var $this PageController */
Yii::import('application.modules.gallery.models.*');

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<h1>
    <a class="back" href="<?= Yii::app()->request->urlReferrer ?>"><img
                src="<?= $this->mainAssets ?>/img/icons/back.svg" alt="Назад"></a>
    <?= $this->title; ?>
</h1>

<?php if (!empty($model->childPages)): ?>
    <?php foreach ($model->childPages as $page): ?>
        <div class="project-item">
            <a href="/<?= $page->slug ?>">
                <h2><?= $page->title ?></h2>

                <?php if (!empty($page->gallery_id)): ?>
                    <?php $gallery = Gallery::model()->findByPk($page->gallery_id); ?>
                    <img src="<?= $gallery->previewImage() ?>" alt="">
                <?php endif; ?>

            </a>
            <a href="/<?= $page->slug ?>" class="btn">Посмотреть</a>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
