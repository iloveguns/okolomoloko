<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<h1 class="contacts-header">
    <a class="back" href="<?= Yii::app()->request->urlReferrer ?>"><img
                src="<?= $this->mainAssets ?>/img/icons/back.svg" alt="Назад"></a>
    <?= $this->title; ?>
</h1>

<div class="catalog-item-page">
    <?= $model->body; ?>
</div>

<?php if (!empty($model->body_2)): ?>
    <div class="catalog-download-review">
        <a href="<?= $model->body_2 ?>" download="Опросный лист. <?= $model->title ?>.docx" class="btn">Скачать опросный лист</a>
        <div>Заполненный лист отправьте, пожалуйста,<br> по адресу <a href="mailto:info@okolomoloko.com">info@okolomoloko.com</a>
        </div>
    </div>
<?php endif; ?>

<!-- Категории -->
<section class="homepage-catalog">
    <?php
    $categoryRepository = new CategoryRepository();
    $catalogCategories = $categoryRepository->getDescendants(1);
    $catalogCategoriesChunks = array_chunk($catalogCategories, 4);
    ?>
    <h2 class="section-header">Продукция</h2>
    <div class="catalog">
        <?php foreach ($catalogCategoriesChunks as $catalogCategoriesChunk): ?>
            <div class="catalog-row">
                <?php foreach ($catalogCategoriesChunk as $catalogCategory): ?>
                    <a href="/katalog-produkcii?category=<?= $catalogCategory->id ?>" class="catalog-item">
                        <img src="<?= $catalogCategory->getImageUrl() ?>" alt="<?= $catalogCategory->name ?>">
                        <p><?= $catalogCategory->name ?></p>
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<script>
    $('table').addClass('table table-striped');
    $('table').wrapAll('<div class="table-responsive">');
</script>