<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<h1 class="contacts-header">
    <a class="back" href="<?= Yii::app()->request->urlReferrer ?>"><img
                src="<?= $this->mainAssets ?>/img/icons/back.svg" alt="Назад"></a>
    <?= $this->title; ?>
</h1>

<div class="catalog-item-page contacts-page">
    <div class="row" style="margin: 0;">
        <?= $model->body; ?>
    </div>

    <div class="contacts-social">
        <a href="#" target="_blank"><img src="<?= $this->mainAssets ?>/img/icons/social/vk.svg" alt="ВКонтакте"></a>
        <a href="#" target="_blank"><img src="<?= $this->mainAssets ?>/img/icons/social/fb.svg" alt="Фэйсбук"></a>
        <a href="#" target="_blank"><img src="<?= $this->mainAssets ?>/img/icons/social/inst.svg" alt="Инстаграм"></a>
    </div>

    <div class="contacts-map">
        <?= $model->body_2; ?>
    </div>
</div>
