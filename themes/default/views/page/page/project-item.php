<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<h1>
    <a class="back" href="<?= Yii::app()->request->urlReferrer ?>"><img
                src="<?= $this->mainAssets ?>/img/icons/back.svg" alt="Назад"></a>
    <?= $this->title; ?>
</h1>

<div class="catalog-item-page project-item-page">
    <?= $model->body; ?>

    <?php if (!empty($model->gallery_id)): ?>
        <!-- Слайдер фото -->
        <?php $this->widget(
            'gallery.widgets.GalleryWidget',
            ['galleryId' => $model->gallery_id, 'limit' => 20, 'view' => 'project-slider']
        ); ?>
    <?php endif; ?>
</div>
