<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$categoryRepository = new CategoryRepository();

if (!empty($_GET['category'])) {
    $category = Category::model()->findByPk($_GET['category']);
    $catalogCategories = [$category];
} else {
    $catalogCategories = $categoryRepository->getDescendants(1);
}

$this->title = !empty($category->name) ? $category->name : 'Каталог продукции';
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<h1>
    <a class="back" href="<?= Yii::app()->request->urlReferrer ?>"><img
                src="<?= $this->mainAssets ?>/img/icons/back.svg" alt="Назад"></a>
    <?= $this->title; ?>
</h1>

<?= $model->body; ?>

<div class="catalog-list" id="accordion">
    <?php foreach ($catalogCategories as $catalogCategory): ?>
        <div class="panel">
            <div class="catalog-list-header">
                <a data-toggle="collapse" data-parent="#accordion"
                   href="#collapse<?= $catalogCategory->id ?>"><?= $catalogCategory->name ?></a>
            </div>
            <div id="collapse<?= $catalogCategory->id ?>" class="panel-collapse collapse">
                <?php $pages = Page::model()->findAllByAttributes(['category_id' => $catalogCategory->id]); ?>
                <?php if ($pages != null): ?>
                    <?php foreach ($pages as $page): ?>
                        <div class="catalog-list-item"><a href="/<?= $page->slug ?>"><?= $page->title ?></a></div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>