<?php
$this->title = Yii::t('ZendSearchModule.zendsearch', 'Search by request: ') . CHtml::encode($term);
$this->breadcrumbs = [
    Yii::t('ZendSearchModule.zendsearch', 'Search by request: ') . CHtml::encode($term),
];
?>

<h1><?= Yii::t('ZendSearchModule.zendsearch', 'Search by request: '); ?> "<?= CHtml::encode($term); ?>"</h1>


<?php if (!empty($results)): ?>
    <?php foreach ($results as $result): ?>
        <?php
        $resultLink = '/';
        $paramsArray = [];

        $linkArray = explode('?', $result->link);
        if (isset($linkArray[0])) {
            $resultLink = $linkArray[0];
        } else {
            $resultLink = $result->link;
        }

        if (isset($linkArray[1])) {
            foreach (explode('&', $linkArray[1]) as $param) {
                $paramArray = explode('=', $param);
                $paramsArray[$paramArray[0]] = $paramArray[1];
            }
        }
        ?>

        <h3>
            <?= $query->highlightMatches(
                CHtml::link(CHtml::encode($result->title), CController::CreateUrl($resultLink, $paramsArray)),
                'UTF-8'
            ); ?>
        </h3>
        <p><?= $query->highlightMatches($result->description, 'UTF-8'); ?></p>
        <hr/>
    <?php endforeach; ?>

<?php else: ?>
    <p class="error"><?= Yii::t('ZendSearchModule.zendsearch', 'Nothing was found'); ?></p>
<?php endif; ?>
