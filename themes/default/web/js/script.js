$(function () {
    $('.js-project-slider').slick({
        infinite: true,
        arrows: false,
        dots: true,
        slidesToShow: 1,
        autoPlay: true,
        autoplaySpeed: 3000,
        adaptiveHeight: true
    });

    $('.js-homepage-slider').slick({
        infinite: true,
        arrows: false,
        dots: true,
        slidesToShow: 1,
        autoPlay: true,
        autoplaySpeed: 3000,
        adaptiveHeight: true
    });

    $('.js-cert-slider').slick({
        infinite: true,
        arrows: false,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        autoPlay: true,
        autoplaySpeed: 3000,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    });

    $('.js-cert-slider').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: true,
        gallery: {
            enabled: true,
            tCounter: ''
        },
        zoom: {
            enabled: true,
            duration: 300
        }
    });

    $('.js-mobile-search').click(function () {
        $('.js-header-search').toggle(400);
    });

    $('.js-burger').click(function () {
        $('.js-mobile-menu').toggle();
    });
});