<?php

class m181117_104549_add_body_2_to_page extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('yupe_page_page', 'body_2', 'text');
	}

	public function safeDown()
	{

	}
}