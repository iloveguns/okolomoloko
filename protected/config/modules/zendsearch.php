<?php

return [
    'module' => [
        'class' => 'application.modules.zendsearch.ZendSearchModule',
        'searchModels' => [
            'Page' => [
                'path' => 'application.modules.page.models.Page',
                'module' => 'page',
                'titleColumn' => 'title',
                'linkColumn' => 'slug',
                'linkPattern' => '/page/page/view?slug={slug}',
                'textColumns' => 'body,title_short,title',
                'criteria' => [
                    'condition' => 'status = :status',
                    'params' => [
                        ':status' => 1
                    ],
                ],
            ],
        ],
    ],
    'import' => [
        'application.modules.zendsearch.models.*',
    ],
    'component' => [],
    'rules' => [
        '/search' => 'zendsearch/search/search',
    ],
];
